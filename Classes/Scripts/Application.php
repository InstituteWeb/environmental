<?php
namespace InstituteWeb\Environmental\Scripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Environmental Application (config)
 *
 * @package InstituteWeb\Environmental
 */
class Application extends \Symfony\Component\Console\Application
{
    public function __construct($name = 'Environmental for iw_master and TYPO3', $version = 'UNKNOWN')
    {
        parent::__construct($name, $version);

        if (PHP_SAPI !== 'cli') {
            echo 'The command line must be executed with a cli PHP binary! The current PHP sapi type is "' . PHP_SAPI . '".' . PHP_EOL;
            exit(1);
        }

        // Register available commands
        $this->add(new Command\Install());
        $this->add(new Command\Prepare());
        $this->add(new Command\Symlink());
        $this->add(new Command\Welcome());
    }
}
