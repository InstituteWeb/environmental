<?php
namespace InstituteWeb\Environmental\Scripts\Command;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Installs TYPO3 and iw_master on current environment.
 *
 * @package InstituteWeb\Environmental
 */
class Install extends Prepare
{
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('install')
            ->setDescription('Installs TYPO3 and iw_master on current environment.')

            ->addOption('host', 'o', InputOption::VALUE_OPTIONAL, 'Database host', '127.0.0.1')
            ->addOption('user', 'u', InputOption::VALUE_OPTIONAL, 'Database user', '')
            ->addOption('pass', 'p', InputOption::VALUE_OPTIONAL, 'Database credentials', '')
            ->addOption('db', 'd', InputOption::VALUE_OPTIONAL, 'Database to choose', '')
            ->addOption('new-db', 'b', InputOption::VALUE_NONE, 'Set this option if the set database (--db|-d) is not existing and must get created.')
            ->addOption('port', null, InputOption::VALUE_OPTIONAL, 'Optional port to connect to', '3306')
            ->addOption('socket', null, InputOption::VALUE_OPTIONAL, 'Optional socket', '')
            ->addOption('admin-user-name', 'a', InputOption::VALUE_OPTIONAL, 'Username of admin user to create during installation')
            ->addOption('admin-password', 's', InputOption::VALUE_OPTIONAL, 'Password of admin user to create during installation')
        ;
    }

    /**
     * Set application context in .env file,
     * updates vendor and project name in composer.json and
     * installs TYPO3 with iw_master distribution
     *
     * Example call:
     * php_cli bin/config install --context="Development" --vendor="Blah" --project="BlubbBlubb"
     * -u="username" -p="pass" -o="127.0.0.1" -d="database" -a="admin" -s="supersecret"
     *
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initialize($input, $output);

        $this->checkForMissingOptions();
        $this->validateOptions();
        $this->outputCliCallWithEnteredParameters();

        $this->stepSetApplicationContextToDotEnv();
        $this->stepUpdateVendorAndProjectNameInComposerJson();
        $this->stepInstallTypo3();
    }

    /**
     * Checks all inputs and ask for missing ones. If mode is not interactive, it does not ask but validates options,
     * which may result in an exception on validation issues.
     *
     * @return void
     */
    protected function checkForMissingOptions()
    {
        if (!$this->input->isInteractive()) {
            return;
        }

        if (!$this->input->getOption('context')) {
            $this->input->setOption(
                'context',
                $this->ask('Please enter the current application context', 'Development')
            );
        }
        if (!$this->input->getOption('vendor')) {
            $this->input->setOption(
                'vendor',
                $this->ask('Please enter your vendor name', 'VendorName')
            );
        }
        if (!$this->input->getOption('project')) {
            $this->input->setOption(
                'project',
                $this->ask('Please enter the name of your project', 'Project')
            );
        }
        if (!$this->input->getOption('host')) {
            $this->input->setOption(
                'host',
                $this->ask('Enter hostname of database', '127.0.0.1')
            );
        }
        if (!$this->input->getOption('user')) {
            $this->input->setOption(
                'user',
                $this->ask('Enter user of database')
            );
        }
        if (!$this->input->getOption('pass')) {
            $this->input->setOption(
                'pass',
                $this->askHidden('Enter password of database', '', false)
            );
        }
        if (!$this->input->getOption('db')) {
            $this->input->setOption(
                'db',
                $this->ask('Enter database name')
            );
        }
        if (!$this->input->getOption('port')) {
            $this->input->setOption(
                'port',
                $this->ask('Enter port of database', '3306', false)
            );
        }
        if (!$this->input->getOption('admin-user-name')) {
            $this->input->setOption(
                'admin-user-name',
                $this->ask('Enter the username of admin user to create', 'admin')
            );
        }
        if (!$this->input->getOption('admin-password')) {
            $this->input->setOption(
                'admin-password',
                $this->askHidden('Enter the password of the admin user "' . $this->input->getOption('admin-user-name') . '"')
            );
        }
    }

    /**
     * Validates options and returns true/false or if set, throws an exception in false case
     *
     * @param bool $throwException If true a validation errors throws an exception
     * @return bool
     * @throws \InvalidArgumentException
     */
    protected function validateOptions($throwException = true)
    {
        $missingOptions = [];
        if (!$this->input->getOption('context')) {
            $missingOptions[] = 'context';
        }
        if (!$this->input->getOption('host')) {
            $missingOptions[] = 'host';
        }
        if (!$this->input->getOption('user')) {
            $missingOptions[] = 'user';
        }
        if (!$this->input->getOption('db')) {
            $missingOptions[] = 'db';
        }
        if (!$this->input->getOption('admin-user-name')) {
            $missingOptions[] = 'admin-user-name';
        }
        if (!$this->input->getOption('admin-password')) {
            $missingOptions[] = 'admin-password';
        }
        if (!empty($missingOptions)) {
            if ($throwException) {
                throw new \InvalidArgumentException('These options are missing: ' . implode(', ', $missingOptions));
            }
            return false;
        }

        return true;
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\NotImplementedException
     */
    protected function stepInstallTypo3()
    {
        $options = [
            'database-user-name' => $this->input->getOption('user'),
            'database-user-password' => $this->input->getOption('pass') ?: '',
            'database-host-name' => $this->input->getOption('host'),
            'database-port' => $this->input->getOption('port'),
            'database-socket' => $this->input->getOption('socket'),
            'database-name' => $this->input->getOption('db'),
            'use-existing-database' => !$this->input->getOption('new-db'),
            'admin-user-name' => $this->input->getOption('admin-user-name'),
            'admin-password' => $this->input->getOption('admin-password'),
            'site-name' => $this->input->getOption('project'),

            'non-interactive' => '1',
        ];
        $optionLine = '';
        foreach ($options as $key => $value) {
            if ($value !== '') {
                $optionLine .= ' --' . $key . ' ' . $value;
            }
        }

        $command = $this->getBinaryPath('typo3cms') . ' install:setup' . $optionLine;
        if ($this->isWindows()) {
            throw new \TYPO3\CMS\Extbase\Persistence\Generic\Exception\NotImplementedException(
                'On Windows TTY on cli is not supported, so you need to execute it manually: ' . PHP_EOL . '> ' .
                $command . PHP_EOL . PHP_EOL .
                'Please delete web/typo3conf/PackageStates.php after installation (important!) and recreate it with ' . PHP_EOL .
                '> composer dump-autoload' . PHP_EOL . PHP_EOL .

                'Then remove the folder web/typo3temp/Cache/ completely.' . PHP_EOL . PHP_EOL .

                'Also you need to call database schema update script:' . PHP_EOL .
                '> composer run-script post-update-cmd' . PHP_EOL . PHP_EOL .

                'or if you want to call the command directly:' . PHP_EOL .
                '> ' . $this->getBinaryPath('typo3cms') . ' database:updateschema --schema-update-types="*"' . PHP_EOL
            );
        }

        $process = $this->executeProcess($command);
        if ($process->isSuccessful()) {
            $packageStatesPath = realpath($this->input->getOption('cwd') . '/web/typo3conf/PackageStates.php');
            if ($packageStatesPath) {
                unlink($packageStatesPath);
                $this->writeln('Deleted file "' . $packageStatesPath . '".');
            }
            $status = $this->executeProcess($this->getBinaryPath('typo3cms') . ' install:generatepackagestates');
            if ($status->isSuccessful()) {
                $this->writeln('Recreated PackageStates.php');
            }
        }
        if ($process->isSuccessful() && $status->isSuccessful()) {
            //TODO: Make condition for TYPO3 version here (and in help text for windows users)
            $this->writeln('Deleting cache directory...');
            GeneralUtility::rmdir($this->input->getOption('cwd') . '/web/typo3temp/Cache', true);
            GeneralUtility::rmdir($this->input->getOption('cwd') . '/web/typo3temp/var', true);

            $this->writeln('Call database schema updates (twice)...');
            $this->executeProcess($this->getBinaryPath('typo3cms') . ' database:updateschema --schema-update-types="*"');
            $this->executeProcess($this->getBinaryPath('typo3cms') . ' database:updateschema --schema-update-types="*"');
        }
    }

    /**
     * Outputs the possible cli call, with previous entered values.
     * Password values are not added.
     *
     * @return void
     */
    protected function outputCliCallWithEnteredParameters()
    {
        $options = [
            'context' => $this->input->getOption('context'),
            'vendor' => $this->input->getOption('vendor'),
            'project' => $this->input->getOption('project'),
            'user' => $this->input->getOption('user'),
            'host' => $this->input->getOption('host'),
            'port' => $this->input->getOption('port'),
            'socket' => $this->input->getOption('socket'),
            'db' => $this->input->getOption('db'),
            'admin-user-name' => $this->input->getOption('admin-user-name'),
        ];

        $cmd = $this->isWindows() ? 'bin\\config install' : PHP_BINARY . ' bin/config install';
        foreach ($options as $name => $value) {
            if (!empty($value)) {
                $cmd .= ' --' . $name . '=' . escapeshellarg($value);
            } else {
                $cmd .= ' --' . $name;
            }
        }
        if (!$this->input->getOption('new-db')) {
            $cmd .= ' --new-db';
        }

        $this->writeln([
            '',
            'To run this command again you can use this command (does not contain passwords):',
            '$ <info>' . $cmd . '</info>',
            ''
        ]);
    }
}
