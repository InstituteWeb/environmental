<?php
namespace InstituteWeb\Environmental\Scripts\Command;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Creates symlink
 *
 * @package InstituteWeb\Environmental
 */
class Symlink extends AbstractCommand
{
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('symlink')
            ->setDescription('Creates symlinks')

            ->addArgument('source', InputArgument::REQUIRED, 'Path to source. e.g. "web/typo3/"')
            ->addArgument('symlink', InputArgument::REQUIRED, 'Name of symlink. e.g. "typo3"')

            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Overwrites existing symlinks.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initialize($input, $output);

        if (file_exists($input->getArgument('symlink'))) {
            if ($input->getOption('force')) {
                $this->writeln('forcing...');
            } else {
                $this->writeln('Symlink "' . $input->getArgument('symlink') . '" already existing.');
            }
        }

        $command = 'ln -s ' . $input->getArgument('source') . ' ' . $input->getArgument('symlink');
        if ($this->isWindows()) {
            $isDirectory = is_dir($input->getArgument('source')) ? '/D' : '';
            $command = 'mklink ' . $isDirectory . ' ' . $input->getArgument('symlink') . ' ' . $input->getArgument('source');
        }

        $process = new \Symfony\Component\Process\Process($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new \RuntimeException($process->getErrorOutput());
        } else {
            $this->writeln('Symlink successfully created.');
        }
    }

    /**
     * Checks if the current system is Windows
     *
     * @return bool
     */
    protected function isWindows()
    {
        return DIRECTORY_SEPARATOR === '\\';
    }
}
