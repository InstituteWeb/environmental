<?php
namespace InstituteWeb\Environmental\Scripts\Command;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Say welcome to new iw_master users and show how to prepare and install iw_master.
 *
 * @package InstituteWeb\Environmental
 */
class Welcome extends AbstractCommand
{
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('welcome')
            ->setDescription('Say welcome to new iw_master users and show how to prepare and install iw_master.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initialize($input, $output);
        $this->writeln([
            '',
            'Welcome to iw_master!',
            '=====================',
            '',
            'You can start installation anytime with: ',
            '$ <info>' . $this->getBinaryPath('config') . ' install </info>',
            '',
            'In case you just want to prepare the project files (update vendor and project name)',
            'without installing TYPO3 you can use:',
            '$ <info>' . $this->getBinaryPath('config') . ' prepare </info>',
            '',
            'Happy Coding! <3',
        ]);
    }
}
