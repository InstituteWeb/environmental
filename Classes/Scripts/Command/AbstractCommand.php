<?php
namespace InstituteWeb\Environmental\Scripts\Command;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * AbstractCommand
 *
 * @package InstituteWeb\Environmental
 */
abstract class AbstractCommand extends Command
{
    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * Configures this command and registers cwd option,
     * which is used by $this->getCwd();
     *
     * @return void
     */
    protected function configure()
    {
        $this->addOption('cwd', null, InputOption::VALUE_OPTIONAL, 'Overwrite current working directory', getcwd());
    }


    /**
     * The execute method
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $this->initialize($input, $output);
    }

    /**
     * Initializes the command. Required!
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        if (!file_exists($this->getCwd())) {
            throw new \RuntimeException(
                'The given current working directory (' . $this->getCwd() . ') does not exist!'
            );
        }
        if (!$this->cwdHasComposer()) {
            throw new \RuntimeException(
                'The given current working directory (' . $this->getCwd() . ') has no composer.json. ' . PHP_EOL .
                'Please change directory or add --cwd="/path/to/project".'
            );
        }
    }

    /**
     * Writes to output
     *
     * @param string|array $messages
     * @param bool $newline
     * @param int $options
     */
    protected function write($messages, $newline = false, $options = 0)
    {
        $this->output->write($messages, $newline, $options);
    }

    /**
     * Writes to output
     *
     * @param string|array $messages
     * @param int $options
     */
    protected function writeln($messages, $options = 0)
    {
        $this->output->writeln($messages, $options);
    }

    /**
     * Asks the user a question
     *
     * @param string $question
     * @param mixed|null $default
     * @param callable|null|bool $validator If false no validator called.
     * @return string The user input
     */
    protected function ask($question, $default = null, $validator = null)
    {
        $questionText = $question;
        if (!is_null($default)) {
            $questionText = rtrim($questionText, ': ');
            $questionText .= ' [' . $default . ']: ';
        } else {
            $questionText .= ': ';
        }

        $questionInstance = new Question($questionText, $default);
        if (is_null($validator)) {
            $validator = function ($answer) {
                if (!$answer) {
                    throw new \RuntimeException('No value given');
                }
                return $answer;
            };
        }
        if ($validator === false) {
            $validator = null;
        }
        $questionInstance->setValidator($validator);

        /** @var \Symfony\Component\Console\Helper\QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');
        return $questionHelper->ask($this->input, $this->output, $questionInstance);
    }

    /**
     * Ask for configuration (yes/no)
     *
     * @param string $question
     * @param bool $default true
     * @return bool The user input, true or false
     */
    protected function askConfirmation($question, $default = true)
    {
        if ($this->input->hasOption('force') && $this->input->getOption('force')) {
            return true;
        }
        $defaultText = $default ? 'Y/n' : 'y/N';
        $questionInstance = new ConfirmationQuestion($question . ' [' . $defaultText . '] ', $default, '/^y|j/i');
        /** @var \Symfony\Component\Console\Helper\QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');
        return $questionHelper->ask($this->input, $this->output, $questionInstance);
    }

    /**
     * Asks the user. Its input is hidden (if supported from the system)
     *
     * @param string $question
     * @param mixed|null $default
     * @param callable|null|bool $validator If false no validator called.
     * @return string The user input
     */
    protected function askHidden($question, $default = null, $validator = null)
    {
        $questionInstance = new Question($question . ': ', $default);
        $questionInstance->setHidden(true);
        $questionInstance->setHiddenFallback(true);
        if (is_null($validator)) {
            $validator = function ($answer) {
                if (!$answer) {
                    throw new \RuntimeException('No value given');
                }
                return $answer;
            };
        }
        if ($validator === false) {
            $validator = null;
        }
        $questionInstance->setValidator($validator);

        /** @var \Symfony\Component\Console\Helper\QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');
        return $questionHelper->ask($this->input, $this->output, $questionInstance);
    }

    /**
     * Returns binary path, configured in composer.json
     *
     * @param string $binary Name of binary (without file extension!)
     * @return bool|string
     */
    protected function getBinaryPath($binary)
    {
        $composerJson = @json_decode(file_get_contents($this->getCwd() . '/composer.json'), true);
        $binDir = $composerJson && isset($composerJson['config']['bin-dir'])
            ? $composerJson['config']['bin-dir']
            : './vendor/bin';
        $binDir = realpath($this->getCwd() . '/' . $binDir);

        return $this->isWindows() ? $binDir . '\\' . $binary
            : PHP_BINARY . ' ' . $binDir . '/' . $binary;
    }

    /**
     * Runs given command with "php" (PHP_BINARY) prepended and disables timeout
     *
     * @param string $processCall e.g. "iwm entities:update --dry-run"
     * @return \Symfony\Component\Process\Process
     * @throws \Exception
     */
    protected function executeProcess($processCall)
    {
        if ($this->isWindows()) {
            throw new \Exception('Can\'t execute processes on Windows OS, due missing TTY support.');
        }
        $process = new \Symfony\Component\Process\Process(PHP_BINARY . ' ' . $processCall);
        $process->setTimeout(null);
        $process->setTty(true);
        $process->run();
        return $process;
    }

    /**
     * Get current working directory and respects --cwd option
     *
     * @return string
     */
    protected function getCwd()
    {
        $cwd = getcwd();
        if ($this->input->hasOption('cwd') && $this->input->getOption('cwd')) {
            $cwd = $this->input->getOption('cwd');
        }
        return $cwd;
    }

    /**
     * Checks if current working directory (cwd) has a composer.json file
     *
     * @return bool
     */
    protected function cwdHasComposer()
    {
        return file_exists($this->getCwd() . DIRECTORY_SEPARATOR . 'composer.json');
    }

    /**
     * Checks if the current system is Windows
     *
     * @return bool
     */
    protected function isWindows()
    {
        return DIRECTORY_SEPARATOR === '\\';
    }
}
