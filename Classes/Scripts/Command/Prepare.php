<?php
namespace InstituteWeb\Environmental\Scripts\Command;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Prepares iw_master without installing it on current environment
 *
 * @package InstituteWeb\Environmental
 */
class Prepare extends AbstractCommand
{
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('prepare')
            ->setDescription('Prepares iw_master without installing it on current environment.')

            ->addOption('context', 'c', InputOption::VALUE_OPTIONAL, 'The TYPO3_CONTEXT e.g. "Development"')
            ->addOption('vendor', null, InputOption::VALUE_OPTIONAL, 'Your vendor name (in UpperCamelCase) for composer.json')
            ->addOption('project', null, InputOption::VALUE_OPTIONAL, 'Name of the project (in UpperCamelCase) for composer.json')

            ->addOption('force', 'f', InputOption::VALUE_NONE, 'If set all confirmations asked during installation are answered with yes, which continues installation.')
        ;
    }

    /**
     * Set application context in .env file,
     * updates vendor and project name in composer.json and
     * installs TYPO3 with iw_master distribution
     *
     * Example call:
     * php_cli bin/config prepare --context="Development" --vendor="Blah" --project="BlubbBlubb"
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initialize($input, $output);

        $this->checkForMissingOptions();
        $this->validateOptions();
        $this->outputCliCallWithEnteredParameters();

        $this->stepSetApplicationContextToDotEnv();
        $this->stepUpdateVendorAndProjectNameInComposerJson();
        $this->stepPrependProjectToReadme();
        $this->stepGitInit();

        $this->writeln([
            '',
            '<info>Important</info>',
            'Don\'t run this command twice, it will cause unwanted results.'
        ]);
    }


    /**
     * Checks all inputs and ask for missing ones. If mode is not interactive, it does not ask but validates options,
     * which may result in an exception on validation issues.
     *
     * @return void
     */
    protected function checkForMissingOptions()
    {
        if (!$this->input->isInteractive()) {
            return;
        }

        if (!$this->input->getOption('context')) {
            $this->input->setOption(
                'context',
                $this->ask('Please enter the current application context', 'Development')
            );
        }
        if (!$this->input->getOption('vendor')) {
            $this->input->setOption(
                'vendor',
                $this->ask('Please enter your vendor name', 'VendorName')
            );
        }
        if (!$this->input->getOption('project')) {
            $this->input->setOption(
                'project',
                $this->ask('Please enter the name of your project', 'Project')
            );
        }
    }

    /**
     * Validates options and returns true/false or if set, throws an exception in false case
     *
     * @param bool $throwException If true a validation errors throws an exception
     * @return bool
     * @throws \InvalidArgumentException
     */
    protected function validateOptions($throwException = true)
    {
        $missingOptions = [];
        if (!$this->input->getOption('context')) {
            $missingOptions[] = 'context';
        }

        if (!empty($missingOptions)) {
            if ($throwException) {
                throw new \InvalidArgumentException('These options are missing: ' . implode(', ', $missingOptions));
            }
            return false;
        }
        return true;
    }

    /**
     * Step: Set application context to .env file
     *
     * @return void
     */
    protected function stepSetApplicationContextToDotEnv()
    {
        $dotEnvWriter = new \InstituteWeb\Environmental\Scripts\Helper\DotEnvWriter(
            $this->getCwd() . '/.env'
        );

        if ($dotEnvWriter->isNew() && !$this->askConfirmation('Do you want to create .env file?')) {
            return;
        }
        $context = $dotEnvWriter->get('TYPO3_CONTEXT');
        $overwrite = true;
        if ($context) {
            $overwrite = $this->askConfirmation('There is already a TYPO3_CONTEXT set. The current value is "' . $context . '". Overwrite?');
        }
        if ($overwrite) {
            $dotEnvWriter->set('TYPO3_CONTEXT', $this->input->getOption('context'));
            $dotEnvWriter->save();
            $this->writeln('Updated TYPO3_CONTEXT in .env file.');
        }
    }

    /**
     * Step: Update vendor and project name in composer.json
     *
     * @return void
     */
    protected function stepUpdateVendorAndProjectNameInComposerJson()
    {
        $updateVendor = $this->askConfirmation('Update vendor and project name in composer.json?');
        if ($updateVendor) {
            $vendorName = str_replace('_', '-', GeneralUtility::camelCaseToLowerCaseUnderscored($this->input->getOption('vendor')));
            $projectName = str_replace('_', '-', GeneralUtility::camelCaseToLowerCaseUnderscored($this->input->getOption('project')));
            $packageName = $vendorName . '/' . $projectName;

            $composerJsonPath = $this->input->getOption('cwd') . '/composer.json';
            if (!realpath($composerJsonPath)) {
                throw new RuntimeException('File "' . $composerJsonPath . '" not found.');
            }
            $composerJson = file_get_contents($composerJsonPath);

            $replacedContent = preg_replace('/(.*?"name":\s*?")(.*?)(".*)/s', '$1' . $packageName . '$3', $composerJson);
            $replacedContent = str_replace(
                'VendorName\\\\Project\\\\',
                $this->input->getOption('vendor') . '\\\\' . $this->input->getOption('project') . '\\\\',
                $replacedContent
            );

            $status = file_put_contents($composerJsonPath, $replacedContent);
            if (!$status) {
                throw new RuntimeException('Can\'t write to file: ' . $composerJsonPath);
            }
            $this->writeln('Updated composer.json.');
        }
    }

    /**
     * Performs git init, git add --all and created initial commit
     *
     * @return void
     */
    protected function stepGitInit()
    {
        $initGit = $this->askConfirmation(
            'Do you want to <info>git init</info> and add local iw_master files to an initial commit?'
        );
        if ($initGit) {
            $this->git('init');
            $this->git('add --all', false);
            $commitMessage = '[INITIAL] Kickstart project "' . $this->getPackageName() . '"';
            $commitStatus = $this->git('commit -m "' . $commitMessage . '"', true);
            if ($commitStatus) {
                $this->writeln('<info>Commit "' . $commitMessage . '" successfully created.</info>');
            } else {
                $this->writeln('<error>Git commit failed.</error>');
            }

            $this->writeln([
                '',
                'Next steps:',
                '$ <info>git remote add origin https://git@git-hoster.org/vendor/project-name</info>',
                '$ <info>git push --set-upstream origin master</info>'
            ]);
        }
    }

    /**
     * Prepends package name to README.md
     *
     * @return void
     */
    protected function stepPrependProjectToReadme()
    {
        $prependReadme = $this->askConfirmation('Also add vendor and project name to beginning of README.md?');
        if ($prependReadme) {
            $readmeContents = file_get_contents($this->getCwd() . '/README.md');

            $newContent = '# Project ' . $this->getPackageName() . PHP_EOL . PHP_EOL .
                'This project has been created on ' . date('d.m.Y') . ' and is based on ' .
                '[instituteweb/iw_master](https://bitbucket.org/InstituteWeb/iw_master).' .
                PHP_EOL . PHP_EOL;

            $status = file_put_contents($this->getCwd() . '/README.md', $newContent . $readmeContents);
            if ($status) {
                $this->writeln('Updated README.md');
            } else {
                $this->writeln('<error>Error while updating README.md</error>');
            }
        }
    }

    /**
     * Outputs the possible cli call, with previous entered values.
     * Password values are not added.
     *
     * @return void
     */
    protected function outputCliCallWithEnteredParameters()
    {
        $options = [
            'context' => $this->input->getOption('context'),
            'vendor' => $this->input->getOption('vendor'),
            'project' => $this->input->getOption('project'),
        ];

        $cmd = $this->getBinaryPath('config') . ' prepare';
        foreach ($options as $name => $value) {
            if (!empty($value)) {
                $cmd .= ' --' . $name . '=' . escapeshellarg($value);
            } else {
                $cmd .= ' --' . $name;
            }
        }

        $this->writeln([
            '',
            'To run this command again you can use this command (does not contain passwords):',
            '$ <info>' . $cmd . '</info>',
            ''
        ]);
    }

    /**
     * Composer package name with given "vendor" and "project" option
     *
     * @return string
     */
    protected function getPackageName()
    {
        $vendorName = strtolower($this->input->getOption('vendor'));
        $projectName = str_replace('_', '-', GeneralUtility::camelCaseToLowerCaseUnderscored($this->input->getOption('project')));
        return $vendorName . '/' . $projectName;
    }

    /**
     * Executes git command
     *
     * @param string $command
     * @param bool $silent No output if true
     * @return bool
     */
    protected function git($command, $silent = false)
    {
        $git = getenv('GIT_BIN') ?: 'git';
        $process = new \Symfony\Component\Process\Process($git . ' ' . $command, $this->getCwd());
        $process->run();
        if (!$silent) {
            if (!$process->isSuccessful()) {
                throw new \RuntimeException($process->getErrorOutput());
            }
            $this->writeln(trim($process->getOutput()));
        }
        return $process->isSuccessful();
    }
}
