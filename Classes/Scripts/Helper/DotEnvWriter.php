<?php
namespace InstituteWeb\Environmental\Scripts\Helper;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * DotEnvWriter Helper
 *
 * @package InstituteWeb\Environmental
 */
class DotEnvWriter
{
    /**
     * @var array
     */
    protected $environmentVariables = null;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var bool
     */
    protected $new = false;


    /**
     * DotEnvWriter constructor.
     *
     * @param string $path If path not ends with "/.env" it get appended automatically
     */
    public function __construct($path)
    {
        $this->load($path);
    }

    /**
     * Loads given .env file. If not existing but writable, it flags the model as new
     *
     * @param string $path If path not ends with "/.env" it get appended automatically
     * @return DotEnvWriter
     */
    protected function load($path)
    {
        $this->path = $path;

        if (!is_writable(dirname($path))) {
            throw new \RuntimeException('Path "' . dirname($path). '" not writable.');
        }

        $this->environmentVariables = [];

        if (!file_exists($path)) {
            $this->new = true;
            return $this;
        }

        $content = file_get_contents($path);
        foreach (explode(PHP_EOL, $content) as $line) {
            if (strpos($line, '=') !== false) {
                parse_str($line, $splittedLines);
                $this->environmentVariables = array_merge($this->environmentVariables, $splittedLines);
            }
        }
        return $this;
    }

    /**
     * Saves changes to file
     *
     * @return DotEnvWriter
     */
    public function save()
    {
        $variables = [];
        foreach ($this->environmentVariables as $key => $value) {
            $variables[] = $key . '=' . $value;
        }
        $status = @file_put_contents($this->path, implode(PHP_EOL, $variables));
        if (!$status) {
            throw new \RuntimeException('Can\'t write to file: ' . $this->path);
        }
        $this->new = false;
        return $this;
    }

    /**
     * Get value of given key
     *
     * @param string $key
     * @return string|null Value of given key, or null if empty
     */
    public function get($key)
    {
        return isset($this->environmentVariables[$key]) ? $this->environmentVariables[$key] : null;
    }

    /**
     * Set value to given key
     *
     * @param string $key
     * @param string $value
     * @return DotEnvWriter
     */
    public function set($key, $value)
    {
        $this->environmentVariables[strtoupper($key)] = $value;
        return $this;
    }

    /**
     * Removes given key from environment variables
     *
     * @param string $key
     * @return DotEnvWriter
     */
    public function remove($key)
    {
        if (array_key_exists($key, $this->environmentVariables)) {
            unset($this->environmentVariables[$key]);
        }
        return $this;
    }

    /**
     * Get New
     *
     * @return boolean
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Checks if given key exists
     *
     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        return array_key_exists($key, $this->environmentVariables);
    }

    /**
     * Returns path to .env file
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
