<?php
namespace InstituteWeb\Environmental\Scripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Class ComposerEvents
 *
 * @package InstituteWeb\Environmental
 */
class ComposerEvents
{
    /**
     * Stops the current event execution, if no connection to proper TYPO3 database found.
     *
     * @param \Composer\Script\Event $event
     * @return void
     */
    public static function stopIfNoDatabaseConnection(\Composer\Script\Event $event)
    {
        // Assumed location: ./web/typo3conf/ext/environmental/Classes/Scripts/
        $rootDirectory = realpath(__DIR__ . '/../../../../../../');
        if (!$rootDirectory) {
            // Assumed location: ./typo3conf/ext/environmental/Classes/Scripts/
            $rootDirectory = realpath(__DIR__ . '/../../../../../');
        }

        if (!getenv('TYPO3_PATH_WEB')) {
            putenv('TYPO3_PATH_WEB=' . $rootDirectory . '/web');
        }

        if (!defined('PATH_site')) {
            define('PATH_site', str_replace('\\', '/', rtrim(getenv('TYPO3_PATH_WEB'), '\\/')) . '/');
        }
        if (!defined('PATH_thisScript')) {
            define('PATH_thisScript', realpath(PATH_site . 'typo3/cli_dispatch.phpsh'));
        }
        $hasDatabaseConnection = true;
        $errorMessage = null;
        $classLoader = require $rootDirectory . '/vendor/autoload.php';

        \TYPO3\CMS\Core\Utility\GeneralUtility::presetApplicationContext(
            new \TYPO3\CMS\Core\Core\ApplicationContext(getenv('TYPO3_CONTEXT') ?: 'Production')
        );
        try {
            \TYPO3\CMS\Core\Core\SystemEnvironmentBuilder::run();
            $bootstrap = \TYPO3\CMS\Core\Core\Bootstrap::getInstance();
            $bootstrap->initializeClassLoader($classLoader);
            $bootstrap->registerRequestHandlerImplementation(\TYPO3\CMS\Backend\Console\CliRequestHandler::class);
            $bootstrap->loadConfigurationAndInitialize();
            $bootstrap->unsetReservedGlobalVariables();
            $bootstrap->initializeTypo3DbGlobal();
        } catch (\Exception $e) {
            $hasDatabaseConnection = false;
        }

        if ($hasDatabaseConnection && isset($GLOBALS['TYPO3_DB']) && $GLOBALS['TYPO3_DB'] instanceof \TYPO3\CMS\Core\Database\DatabaseConnection) {
            /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $databaseConnection */
            $databaseConnection = $GLOBALS['TYPO3_DB'];
            try {
                $tables = $databaseConnection->admin_get_tables();
            } catch (\Exception $e) {
                $hasDatabaseConnection = false;
            }
            if (empty($tables)) {
                $hasDatabaseConnection = false;
            }
        }

        if (!$hasDatabaseConnection) {
            $event->stopPropagation();
            echo 'Stopped ' . ltrim($event->getName(), '_') . '.' . PHP_EOL;
        }
        else {
            echo "Continuing..." . PHP_EOL;
        }
    }


    const ALIAS_EVENT_NAME_PREFIX = '_';

    /**
     * Applies improved script execution for all following scripts in this batch
     *
     * @param \Composer\Script\Event $event
     * @return void
     * @see https://bitbucket.org/InstituteWeb/composer-scripts
     */
    public static function ImprovedScriptExecution(\Composer\Script\Event $event)
    {
        $improvedScriptExecution = new \InstituteWeb\ComposerScripts\ImprovedScriptExecution();
        $improvedScriptExecution->apply($event, __METHOD__);
    }
}
