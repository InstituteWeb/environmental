<?php
namespace InstituteWeb\Environmental;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Core\Core\ApplicationContext;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helper class for different environments based on ApplicationContexts
 *
 * @package Iwm\Iwm
 */
class Environment implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var array|null
     */
    public $typo3ConfVars = [];

    /**
     * @var ApplicationContext
     */
    private $applicationContext;

    /**
     * @var string Path where environment configurations are stored
     */
    private $configurationPath;


    /**
     * Environment constructor
     *
     * @param string $configurationPath Path to directory which contains the environment configurations
     * @param ApplicationContext $applicationContext
     * @param array $typo3ConfVars
     */
    public function __construct($configurationPath, ApplicationContext $applicationContext, $typo3ConfVars = null)
    {
        if (!is_null($typo3ConfVars) && empty($this->typo3ConfVars)) {
            $this->typo3ConfVars = $typo3ConfVars;
        }
        $this->applicationContext = $applicationContext;
        $this->configurationPath = $configurationPath;
    }

    /**
     * Loads configuration file from set folder, by given Application Context
     *
     * @return array
     */
    public function load()
    {
        // Call default file, if existing
        $defaultPath = $this->configurationPath . 'Default.php';
        $realDefaultPath = realpath($defaultPath);
        if ($realDefaultPath) {
            $this->callPhpClosureConfig($realDefaultPath);
        }

        // Call current environment configuration
        $includePath = $this->configurationPath . (string) $this->applicationContext . '.php';
        $realIncludePath = realpath($includePath);
        if ($realIncludePath) {
            $this->callPhpClosureConfig($realIncludePath);
        }
        // TODO: Report error in case of missing config file
        return $this->typo3ConfVars;
    }

    /**
     * Includes given php file. Expecting and executing a returning closure function.
     * Passing $this (Environment) to closure as only parameter.
     *
     * @param string $pathToPhpFile Which contains a returning closure function
     * @return callable
     */
    public function callPhpClosureConfig($pathToPhpFile)
    {
        $path = realpath($pathToPhpFile);
        if (!$path) {
            throw new \RuntimeException('File not found! File: ' . $pathToPhpFile);
        }
        $phpResult = require ($path);
        if (!is_callable($phpResult)) {
            throw new \RuntimeException('This file does not return a callable function. File: ' . $path);
        }
        return $phpResult($this);
    }

    /**
     * Includes given php file. Expecting a returning array
     *
     * @param string $pathToPhpFile Which contains a returning array
     * @return array
     */
    public function callPhpArrayConfig($pathToPhpFile)
    {
        $path = realpath($pathToPhpFile);
        if (!$path) {
            throw new \RuntimeException('File not found! File: ' . $pathToPhpFile);
        }
        $phpResult = require ($path);
        if (!is_array($phpResult)) {
            throw new \RuntimeException('This file does not return an array. File: ' . $path);
        }
        return $phpResult;
    }

    /**
     * Compares given contextPattern with current application context
     *
     * @param string $contextPattern Simple string (with wildcard * support) or regular expression.
     *                               You can also pass multiple patterns, separated by comma.
     * @return bool Returns true if given pattern matches with current application context
     */
    public function matchApplicationContext($contextPattern)
    {
        $values = GeneralUtility::trimExplode(',', $contextPattern, true);
        foreach ($values as $applicationContext) {
            if (static::searchStringWildcard($this->applicationContext, $applicationContext)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Matching two strings against each other, supporting a "*" wildcard
     * or (if wrapped in "/") PCRE regular expressions
     *
     * @param string $haystack
     * @param string $needle
     * @return bool
     *
     * @see \TYPO3\CMS\Core\Configuration\TypoScript\ConditionMatching\AbstractConditionMatcher::searchStringWildcard
     */
    protected function searchStringWildcard($haystack, $needle)
    {
        $result = false;
        if ($haystack === $needle) {
            $result = true;
        } elseif ($needle) {
            if (preg_match('/^\\/.+\\/$/', $needle)) {
                // Regular expression, only "//" is allowed as delimiter
                $regex = $needle;
            } else {
                $needle = str_replace(array('*', '?'), array('###MANY###', '###ONE###'), $needle);
                $regex = '/^' . preg_quote($needle, '/') . '$/';
                // Replace the marker with .* to match anything (wildcard)
                $regex = str_replace(array('###MANY###', '###ONE###'), array('.*', '.'), $regex);
            }
            $result = (bool)preg_match($regex, $haystack);
        }
        return $result;
    }

    /**
     * Set credentials for TYPO3 DatabaseConnection
     *
     * @param string $username
     * @param string $password
     * @param string $database
     * @param string $host
     * @param string $port
     * @param string $socket
     * @return void
     */
    public function setDatabaseCredentials($username, $password, $database, $host = '127.0.0.1', $port = '3306', $socket = '')
    {
        $this->typo3ConfVars['DB']['host'] = $host;
        $this->typo3ConfVars['DB']['username'] = $username;
        $this->typo3ConfVars['DB']['password'] = $password;
        $this->typo3ConfVars['DB']['database'] = $database;
        $this->typo3ConfVars['DB']['port'] = $port;
        $this->typo3ConfVars['DB']['socket'] = $socket;
    }

    /**
     * Sets given setting of extension configuration
     *
     * @param string $extensionKey
     * @param string $settingName
     * @param mixed $value
     * @return void
     */
    public function setExtensionSetting($extensionKey, $settingName, $value)
    {
        $extensionKey = trim($extensionKey);
        $settingName = trim($settingName);

        if (isset($this->typo3ConfVars['EXT']['extConf'][$extensionKey])) {
            $extensionConfiguration = unserialize($this->typo3ConfVars['EXT']['extConf'][$extensionKey]);
            if (is_array($extensionConfiguration)) {
                $extensionConfiguration[$settingName] = $value;
            }
        } else {
            $extensionConfiguration[$settingName] = $value;
        }
        if (is_array($extensionConfiguration)) {
            $this->typo3ConfVars['EXT']['extConf'][$extensionKey] = serialize($extensionConfiguration);
        }
    }

    /**
     * Adds given extensionKey to runtimeActivatedPackages
     *
     * @param string $extensionKey
     * @return void
     */
    public function activateExtension($extensionKey)
    {
        if (!isset($this->typo3ConfVars['EXT']['runtimeActivatedPackages'])) {
            $this->typo3ConfVars['EXT']['runtimeActivatedPackages'] = [];
        }
        $this->typo3ConfVars['EXT']['runtimeActivatedPackages'][] = $extensionKey;
    }

    /**
     * Checks if given extension is existing and set in PackageStates.php
     *
     * @param string $extensionKey
     * @return bool
     */
    protected function isExtensionValid($extensionKey)
    {
        if (!file_exists(realpath(TYPO3_typo3conf . 'ext/' . $extensionKey . '/ext_emconf.php'))) {
            return false;
        }
        $packageStates = @require(TYPO3_typo3conf . 'PackageStates.php');
        return is_array($packageStates) && isset($packageStates['packages'][$extensionKey]['state']);
    }

    /**
     * Get singleton instance of Environment
     *
     * @param string $configurationPath
     * @param ApplicationContext $applicationContext
     * @param array $typo3ConfVars
     * @return Environment
     */
    public static function getInstance(
        $configurationPath,
        ApplicationContext $applicationContext,
        array $typo3ConfVars = null
    ) {
        return GeneralUtility::makeInstance(static::class, $configurationPath, $applicationContext, $typo3ConfVars);
    }
}
