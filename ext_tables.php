<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$boot = function ($extensionKey) {
    $GLOBALS['TBE_STYLES']['skins'][$extensionKey] = array (
        'name' => $extensionKey,
        'stylesheetDirectories' => array(
            'all' => 'EXT:' . $extensionKey . '/Resources/Public/Stylesheet/All/'
        )
    );
    if (!\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isProduction()) {
        $GLOBALS['TBE_STYLES']['skins'][$extensionKey]['stylesheetDirectories']['dev'] =
            'EXT:' . $extensionKey . '/Resources/Public/Stylesheet/Dev/';

    }
};
$boot($_EXTKEY);
unset($boot);
